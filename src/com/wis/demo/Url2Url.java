package com.wis.demo;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.wis.constant.Config;
import com.wis.util.rpc.HttpClient;

public class Url2Url {

	/**
	 * @author vincent ln1394@163.com
     * @date 2017-9-24
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String p1 = "http://i.wisvision.cn:8080/product/1.jpg";
		String p2 = "http://i.wisvision.cn:8080/product/0.jpg";
		System.out.println(compare(p1,p2)); 
	}
	/*
	 * @param picUrl1,picUrl2
	 */
	public static String compare(String picUrl1,String picUrl2){
		String img = "" ;
		//调用开放平台人脸识别服务	    	
		Map map = new HashMap();
		map.put("url1", picUrl1);
		map.put("url2", picUrl2);
		img = HttpClient.sendHttpPostJson(Config.url2url,JSONObject.fromObject(map).toString());
		return img;
	}

}
