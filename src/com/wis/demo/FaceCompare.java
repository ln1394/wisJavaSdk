package com.wis.demo;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wis.constant.Config;
import com.wis.util.rpc.HttpClient;

public class FaceCompare {

	/**
	 * @author vincent ln1394@163.com
     * @date 2017-9-24
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String picUrl1 = "/Users/didi/Downloads/21103451_1.jpg";
		String feature1 = FaceDetect.detect(picUrl1);
		JSONObject obj = JSONObject.fromObject(feature1);
		feature1 = String.valueOf(obj.getJSONArray("facemodels").getJSONObject(0).get("feature"));
		System.out.println(feature1); 
		//服务器压力顶不住
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String picUrl2 = "/Users/didi/Downloads/21103451_1_2.jpg";
		String feature2 = FaceDetect.detect(picUrl2);
	    obj = JSONObject.fromObject(feature2);	    
		feature2 = String.valueOf(obj.getJSONArray("facemodels").getJSONObject(0).get("feature"));
		//服务器压力顶不住
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(compare(feature1,feature2)); 
	}
	/*
	 * @param feature1,feature2
	 */
	public static String compare(String feature1,String feature2){
		System.out.println("param1"+feature1);
		System.out.println("param2"+feature2);
		String img = "" ;
		//调用开放平台人脸识别服务	    	
		Map map = new HashMap();
		map.put("feature1", feature1);
		map.put("feature2", feature2);
		img = HttpClient.sendHttpPostJson(Config.facecompare,JSONObject.fromObject(map).toString());
		return img;
	}

}
