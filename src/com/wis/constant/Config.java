package com.wis.constant;

public class Config {
	//这个是申请的平台的ID
    public static String appid = "0c03f5b9efbc418e9462e011a2a719d4";
    public static String host = "http://api.qiansou.cn/api/";
    public static String facedetect = host + "facedetectv4?appid="+appid;
    public static String facecompare = host + "facecomparev4?appid="+appid;
    public static String face2face = host + "face2facev4?appid="+appid;
    public static String url2url = host + "url2urlv4?appid="+appid;

}
