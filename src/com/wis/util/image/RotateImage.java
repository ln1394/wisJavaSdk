package com.wis.util.image;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Tag;


public class RotateImage {
	public static int rotate(File file){
		BufferedImage src;
		try {
			int angel = getRotateAngleForPhoto(file);
			if(angel == 0){
				return 0;
			}
			src = ImageIO.read(file);
			BufferedImage des = RotateImage.Rotate(src, angel);
			ImageIO.write(des,getExtName(file),file); 
			return 0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		return 1;
	}
	public static BufferedImage transfer(File file){
		BufferedImage src;
		try {
			int angel = getRotateAngleForPhoto(file);
			if(angel == 0){
				return null;
			}
			src = ImageIO.read(file);

			BufferedImage des = RotateImage.Rotate(src, angel);
			return des;
			//ImageIO.write(des,"jpg", new File("C:\\Users\\Administrator\\Desktop\\IMG_0362.JPG")); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		return null;
	}
	public static BufferedImage Rotate(Image src, int angel) {  
		int src_width = src.getWidth(null);  
		int src_height = src.getHeight(null);  
		// calculate the new image size  
		Rectangle rect_des = CalcRotatedSize(new Rectangle(new Dimension(  
				src_width, src_height)), angel);  

		BufferedImage res = null;  
		res = new BufferedImage(rect_des.width, rect_des.height,  
				BufferedImage.TYPE_INT_RGB);  
		Graphics2D g2 = res.createGraphics();  
		// transform  
		g2.translate((rect_des.width - src_width) / 2,  
				(rect_des.height - src_height) / 2);  
		g2.rotate(Math.toRadians(angel), src_width / 2, src_height / 2);  

		g2.drawImage(src, null, null);  
		return res;  
	}  

	public static Rectangle CalcRotatedSize(Rectangle src, int angel) {  
		// if angel is greater than 90 degree, we need to do some conversion  
		if (angel >= 90) {  
			if(angel / 90 % 2 == 1){  
				int temp = src.height;  
				src.height = src.width;  
				src.width = temp;  
			}  
			angel = angel % 90;  
		}  

		double r = Math.sqrt(src.height * src.height + src.width * src.width) / 2;  
		double len = 2 * Math.sin(Math.toRadians(angel) / 2) * r;  
		double angel_alpha = (Math.PI - Math.toRadians(angel)) / 2;  
		double angel_dalta_width = Math.atan((double) src.height / src.width);  
		double angel_dalta_height = Math.atan((double) src.width / src.height);  

		int len_dalta_width = (int) (len * Math.cos(Math.PI - angel_alpha  
				- angel_dalta_width));  
		int len_dalta_height = (int) (len * Math.cos(Math.PI - angel_alpha  
				- angel_dalta_height));  
		int des_width = src.width + len_dalta_width * 2;  
		int des_height = src.height + len_dalta_height * 2;  
		return new Rectangle(new Dimension(des_width, des_height));  
	} 
	
	public static int getRotateAngleForPhoto(File file){  
	     
	    int angel = 0;  
	    int angelType = 0;
	    Metadata metadata = null;  
	    try{  
	        try {
				metadata = JpegMetadataReader.readMetadata(file);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return angel;
			}  
	        for (Directory directory : metadata.getDirectories()) {
	            for (Tag tag : directory.getTags()) {
	                //System.out.println(tag.getTagType()+""+tag.getDescription());
	                //System.out.println(tag.getTagType());
	                if(tag.getTagType()==274){
	                	String temp = tag.getDescription();
	                	int s = temp.indexOf("Rotate");
	                	angel = Integer.parseInt(temp.substring(s+7,temp.lastIndexOf(" ")));
	                	System.out.println("rotate value:"+angel);
	                }
	            }
	            
	            try {
	            	if(directory.containsTag(274)){
	            		angelType = directory.getInt(274);
	            		System.out.println("rotate type:"+angelType);
	            	}
				} catch (MetadataException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	        
	        }
	       
	    } catch(JpegProcessingException e){  
	        e.printStackTrace();  
	    } 
	    //System.out.println("ͼƬ��ת�Ƕȣ�" + angel);  
	    return angel;  
	}  
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String fileName = "/Users/didi/Downloads/21103451_1";
		String ext = "jpg";
		//getRotateAngleForPhoto(fileName+"."+ext);
		//System.exit(0);
		System.out.println("start");
		BufferedImage img = RotateImage.transfer(new File(fileName+"."+ext));
		if(null != img){
			ImageIO.write(img,"jpg", new File(fileName+"_new"+"."+ext)); 
		}
		getRotateAngleForPhoto(fileName+"_new"+"."+ext);
        System.out.println("done");
	}
	public static int getRotateAngleForPhoto(String fileName)throws Exception{  
	      
	    File file = new File(fileName);  
	      
	    int angel = 0;  
	    Metadata metadata;  
	      
	    try{  
	        metadata = JpegMetadataReader.readMetadata(file);  
	        print(metadata, "Using JpegMetadataReader");
	       
	    } catch(JpegProcessingException e){  
	        e.printStackTrace();  
	    }  
	    System.out.println("ͼƬ��ת�Ƕȣ�" + angel);  
	    return angel;  
	}  
	private static void print(Metadata metadata, String method)
    {
        System.out.println();
        System.out.println("-------------------------------------------------");
        System.out.print(' ');
        System.out.print(method);
        System.out.println("-------------------------------------------------");
        System.out.println();

        //
        // A Metadata object contains multiple Directory objects
        //
        for (Directory directory : metadata.getDirectories()) {
            //
            // Each Directory stores values in Tag objects
            //
            for (Tag tag : directory.getTags()) {
                //System.out.println(tag.getTagType()+""+tag.getDescription());
                //System.out.println(tag.getTagType());
                if(tag.getTagType()==274){
                	String temp = tag.getDescription();
                	int s = temp.indexOf("Rotate");
                	System.out.println(temp.substring(s+7,temp.lastIndexOf(" ")));
                }
            }
            
            try {
            	if(directory.containsTag(274)){
            		System.out.println(directory.getInt(274));
            	}
            } catch (MetadataException e) {
            	// TODO Auto-generated catch block
				e.printStackTrace();
			}
            //
            // Each Directory may also contain error messages
            //
            for (String error : directory.getErrors()) {
                System.err.println("ERROR: " + error);
            }
        }
    }
	public static String getExtName(File file){
	      String fileName=file.getName();
	      String prefix=fileName.substring(fileName.lastIndexOf(".")+1);
	      System.out.println(prefix);
	      return prefix;
	}
}
